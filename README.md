# recordparser

The *recordparser* is a LLVM based tool to recursively extract the fields of a typedef'd struct with a specific name.
This tool was meant to automate the tedious task to create a top level VHDL wrapper file for an Altera Avalon Memory Mapped peripheral.
This tool creates the register definition as well as the aliases that are defined in the given header file.
It also creates templates for the Avalon MM read and write processes (in VHDL).

# Dependencies

For dumping the internal register definition to a JSON, [nlohmann's JSON library](https://github.com/nlohmann/json) is used.  
For formatting string formatting the [C++ Format Library - {fmt}](https://github.com/fmtlib/fmt) is used.  
For building recordparser and llvm CMake and Ninja are required. 

# Building

This tool runs on all major operating systems
    
- Windows 10 (tested on the ridiculously old 2016 LTSB and Win10 Version 2004)
- Linux (tested Ubuntu 20.04)
  - LLVM 11 won't build on Ubuntu < 20.04
- MacOS (tested MacOS 10.15)

**Note: This tool has to be built with the same build type like LLVM.
Eg. I compiled LLVM with a Release config (-DCMAKE_BUILD_TYPE=RelWithDebInfo).
Therefore, recordparser has to be built as well in any of the available release types**

Open a 64 bit developer command prompt (```x64 Native Tools Command Prompt for VS 2019```)

	$ git clone https://github.com/llvm/llvm-project.git
	$ cd llvm-project
	$ git checkout llvmorg-11.0.0-rc6
	$ mkdir build
	$ cd build
	$ cmake -G Ninja -DLLVM_ENABLE_PROJECTS='clang;clang-tools-extra;libcxx;libcxxabi;libunwind;lldb;compiler-rt;lld;polly;debuginfo-tests' -DCMAKE_INSTALL_PREFIX=E:\Applications\llvm-11 -DCMAKE_BUILD_TYPE=Release ../llvm
	$ cmake --build .
	$ cmake --install .


## Building recordparser itself

**Configuring and Building:**

    $ mkdir build
    $ cd build
    $ cmake -G Ninja -DCMAKE_BUILD_TYPE=RelWithDebInfo -DLLVM_DIR=[llvm-installation]/lib/cmake/llvm -DClang_DIR=[llvm-installation]/lib/cmake/clang ..
    $ cmake --build .

> You might have to specify the directories for the fmt lib and nlohmann's json lib.
> Check the cmake configure output!

# Example

Assume the definition of the peripheral in the file `example/example.h`.

```c++
#include <stdint.h>

typedef union {
	unsigned int reg;
	struct {
		unsigned int field1 : 1; /**< comment for field1*/
		unsigned int field2 : 1;
		unsigned int field3 : 4; /**< default: 3; comment for field3*/
		unsigned int field4 : 2; /**< default:2, comment for field3*/
	};
} status_t;

typedef struct {
	unsigned int enable : 1;  /**< default=1,Enable description comment*/
	uint16_t	 data	: 16; /**< Default : 0xff, data to store*/
	status_t	 status;	  /**< status of process*/
} volatile example_t;
```

`example_t` is the name of the top level struct that defines the peripheral.
This name is passed to the tool via the command 
Note: The double dash at the end is required!

## Output a VHDL Avalon MM Wrapper

To generate a VHDL file use

	$ recordparser -m example_t -o example/example.vhd example/example.h --

This will generate the file `example/example.vhd` which has a vhdl entity template with the avalon read and write processes and the register description

```vhdl
[...]
type register_t is array (0 to 2) of std_logic_vector(31 downto 0);
signal regs : register_t := (others => (others => '0'));

alias reg_enable : std_logic_vector(31 downto 0) is regs(0);
alias reg_data : std_logic_vector(31 downto 0) is regs(1);
alias reg_status : std_logic_vector(31 downto 0) is regs(2);

alias bf_enable_value : std_logic(0) is reg_enable(0);
alias bf_enable_unused : std_logic is reg_enable(31 downto 1);

alias bf_data_value : std_logic_vector(15 downto 0) is reg_data(15 downto 0);
alias bf_data_unused : std_logic_vector(15 downto 0) is reg_data(31 downto 16);

alias bf_status_field1 : std_logic is reg_status(0);
alias bf_status_field2 : std_logic is reg_status(1);
alias bf_status_field3 : std_logic_vector(3 downto 0) is reg_status(5 downto 2);
alias bf_status_unused_31_to_6 : std_logic_vector(25 downto 0) is reg_status(31 downto 6);
[...]
```

## Output a JSON file

To generate a JSON file use

	$ recordparser -m example_t -f json -o example\example.json example\example.h --

This will generate a file called `example/example.json` with the content

```json
{
    "align": 32,
    "registers": [
        {
            "enable": {
                "default": 1,
                "description": "default=1,Enable description comment",
                "width": 1
            }
        },
        {
            "data": {
                "default": 255,
                "description": "Default : 0xff, data to store",
                "width": 16
            }
        },
        {
            "status": {
                "aliases": [
                    {
                        "": [
                            {
                                "field1": {
                                    "bitwidth": 1,
                                    "description": "comment for field1"
                                }
                            },
                            {
                                "field2": {
                                    "bitwidth": 1
                                }
                            },
                            {
                                "field3": {
                                    "bitwidth": 4,
                                    "default": 3,
                                    "description": "default: 3; comment for field3"
                                }
                            },
                            {
                                "field4": {
                                    "bitwidth": 2,
                                    "default": 2,
                                    "description": "default:2, comment for field3"
                                }
                            }
                        ]
                    }
                ],
                "default": 140,
                "description": "status of process",
                "width": 32
            }
        }
    ]
}

```