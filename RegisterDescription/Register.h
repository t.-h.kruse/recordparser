#ifndef REGISTER_H_
#define REGISTER_H_

#include <string>
#include <vector>

#include "Alias.h"
#include "BitField.h"
#include "nlohmann/json.hpp"

class Register {
private:
	unsigned int m_width;
	unsigned int m_amount;		   /**< default: 1. if m_amount > 1 then this is an array*/
	unsigned int m_defaultValue;   /**< Parsed from the comment / description*/
	unsigned int m_defaultAddress; /**< Parsed from the top level struct's comment / description via @0x...*/
	bool		 m_hasDefaultAddress;
	// vector with alias, bitfield
	bool			   m_signed;
	std::string		   m_name;
	std::string		   m_description;
	std::vector<Alias> m_aliases;

public:
	Register(const std::string& name, unsigned int width, bool isSigned, const std::string& description);
	Register();
	Register& operator=(const Register& rhs);

	std::string	 name() const;
	void		 setName(const std::string& name);
	unsigned int width() const;
	void		 setWidth(unsigned int width);
	std::string	 description() const;
	void		 setDescription(const std::string& description);
	bool		 hasDescription() const;
	void		 asAlignedRegisters(unsigned int align, std::vector<Register>& alignedRegisters) const;
	/**
	 * @brief Calculates the maximum number of bitfields in the register
	 *
	 */
	unsigned int compressedSize() const;
	bool		 isSigned() const;
	bool		 isArray() const;
	unsigned int arraySize() const;
	void		 setArraySize(unsigned int newSize);
	void		 setSigned(bool s);
	unsigned int defaultValue() const;
	void		 setDefaultValue(unsigned int newValue);
	bool		 hasDefaultAddress() const;
	unsigned int defaultAddress() const;
	void		 setDefaultAddress(unsigned int newValue);
	unsigned int mask() const;

	const Alias*			 alias(const std::string& alias) const;
	bool					 hasAliases() const;
	const std::vector<Alias> aliases() const;
	void					 setAliases(const std::vector<Alias>& aliases);

	std::string toVHDL(unsigned int reg_offset, unsigned int align) const;

	Alias*		 addAlias(const std::string& alias);
	Alias*		 appendAlias(const Alias& alias);
	void		 appendBitField(const BitField& bf);
	const Alias& firstAlias() const;
	unsigned int aliasCount() const;

	unsigned int size(unsigned int align) const;

	std::vector<Alias>::iterator			   begin();
	std::vector<Alias>::iterator			   end();
	std::vector<Alias>::const_iterator		   begin() const;
	std::vector<Alias>::const_iterator		   end() const;
	std::vector<Alias>::reverse_iterator	   rbegin();
	std::vector<Alias>::reverse_iterator	   rend();
	std::vector<Alias>::const_reverse_iterator rbegin() const;
	std::vector<Alias>::const_reverse_iterator rend() const;
};

extern void to_json(nlohmann::json& j, const Register& reg);
extern void from_json(const nlohmann::json& j, Register& reg);

#endif