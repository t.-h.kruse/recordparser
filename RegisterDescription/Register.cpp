#include "Register.h"

std::string Register::toVHDL(unsigned int reg_offset, unsigned int align) const {
	std::string out;

	unsigned int num_regs = size(align);

	if (num_regs == 1) {
		out += "alias reg_" + m_name + " : std_logic_vector(" + std::to_string(align - 1) + " downto 0) is regs(" + std::to_string(reg_offset) + ");";
	} else {
		if (const Alias* anon_alias = alias("")) {
			unsigned i = 0;
			for (const BitField& bf : *anon_alias) {
				// for (unsigned int i = 0; i < num_regs; i++) {
				// const BitField& bf = anon_alias->bitfields()[i];
				out += "alias reg_" + m_name + "_" + bf.name() + " : std_logic_vector(" + std::to_string(align - 1) + " downto 0) is regs(" +
					   std::to_string(reg_offset + i) + ");";
				i++;
				if (i < num_regs) out += "\n";
			}
		}
	}

	return out;
}

Register::Register(const std::string& name, unsigned int width, bool isSigned, const std::string& description)
	: m_width(width)
	, m_amount(1)
	, m_defaultValue(0)
	, m_defaultAddress(0)
	, m_hasDefaultAddress(false)
	, m_signed(isSigned)
	, m_name(name)
	, m_description()
	, m_aliases() {
	setDescription(description);
}
Register::Register()
	: m_width(0), m_amount(1), m_defaultValue(0), m_defaultAddress(0), m_hasDefaultAddress(false), m_signed(false), m_name(), m_description(), m_aliases() {}

Register& Register::operator=(const Register& rhs) {
	if (&rhs != this) {
		m_width		   = rhs.width();
		m_amount	   = rhs.arraySize();
		m_defaultValue = rhs.defaultValue();
		m_signed	   = rhs.isSigned();
		m_name		   = rhs.name();
		m_description  = rhs.description();
		m_aliases	   = rhs.aliases();
	}
	return *this;
}

unsigned int Register::defaultValue() const {
	if (hasAliases()) {	 // first alias that is != 0 has precedence
		for (const Alias& alias : m_aliases) {
			unsigned int val = alias.defaultValue();
			if (val != 0) return val;
		}
	}
	return m_defaultValue;
}

void		 Register::setDefaultValue(unsigned int newValue) { m_defaultValue = newValue; }
bool		 Register::hasDefaultAddress() const { return m_hasDefaultAddress; }
unsigned int Register::defaultAddress() const { return m_defaultAddress; }
void		 Register::setDefaultAddress(unsigned int newValue) {
	m_hasDefaultAddress = true;
	m_defaultAddress	= newValue;
}

unsigned int Register::mask() const {
	unsigned int mask = 0;
	for (const Alias& alias : m_aliases) {
		unsigned int aliasMask = alias.mask();
		if (aliasMask > mask) mask = aliasMask;
	}
	return mask;
}

std::string Register::name() const { return m_name; }

unsigned int Register::width() const { return m_width; }

std::string Register::description() const { return m_description; }
bool		Register::hasDescription() const { return !m_description.empty(); }
bool		Register::isSigned() const { return m_signed; }
void		Register::setSigned(bool s) { m_signed = s; }

const Alias* Register::alias(const std::string& alias) const {
	for (const auto& a : m_aliases) {
		if (a.name() == alias) {
			return &a;
		}
	}
	return nullptr;
}
Alias* Register::appendAlias(const Alias& alias) {
	m_aliases.push_back(alias);
	return &m_aliases.back();
}

unsigned int Register::aliasCount() const { return static_cast<unsigned int>(m_aliases.size()); }

const Alias& Register::firstAlias() const { return m_aliases.front(); }

bool Register::hasAliases() const { return (m_aliases.size() > 0) ? true : false; }

const std::vector<Alias> Register::aliases() const { return m_aliases; }

Alias* Register::addAlias(const std::string& alias) {
	m_aliases.push_back(Alias(alias));
	return &m_aliases.back();
}
void Register::appendBitField(const BitField& bf) { m_aliases.back().appendBitField(bf); }

unsigned int Register::size(unsigned int align) const {
	div_t d = div((int)m_width, align);
	if (d.rem > 0)
		return d.quot + 1;
	else
		return d.quot;
}

unsigned int Register::compressedSize() const {
	unsigned int maxAliasWidth = 0;
	if (this->hasAliases()) {
		for (auto& a : m_aliases) {
			unsigned int bitsForAlias = a.totalBitFieldWidth();
			if (bitsForAlias > maxAliasWidth) maxAliasWidth = bitsForAlias;
		}
	} else {
		maxAliasWidth = this->m_width;
	}
	return maxAliasWidth;
}

std::vector<Alias>::iterator	   Register::begin() { return m_aliases.begin(); }
std::vector<Alias>::iterator	   Register::end() { return m_aliases.end(); }
std::vector<Alias>::const_iterator Register::begin() const { return m_aliases.begin(); }
std::vector<Alias>::const_iterator Register::end() const { return m_aliases.end(); }

std::vector<Alias>::reverse_iterator	   Register::rbegin() { return m_aliases.rbegin(); }
std::vector<Alias>::reverse_iterator	   Register::rend() { return m_aliases.rend(); }
std::vector<Alias>::const_reverse_iterator Register::rbegin() const { return m_aliases.rbegin(); }
std::vector<Alias>::const_reverse_iterator Register::rend() const { return m_aliases.rend(); }

void Register::setAliases(const std::vector<Alias>& aliases) { m_aliases = aliases; }
void Register::setDescription(const std::string& description) {
	m_description = description;
	// extract default value from the description string
	std::regex	re("default\\s*[:=]\\s*([[:xdigit:]]|-?)x?[[:xdigit:]]*", std::regex_constants::icase);
	std::smatch m;
	std::regex_search(m_description, m, re);
	if (!m.empty()) {
		std::string str		  = m[0].str();
		size_t		splitHere = str.find_last_of(" =:");
		if (splitHere != std::string::npos) {
			std::string defaultValueStr = str.substr(splitHere + 1, std::string::npos);
			m_defaultValue				= strtoul(defaultValueStr.c_str(), nullptr, 0);
		}
	}

	re = std::regex("@0x[0-9a-f]+", std::regex_constants::icase);
	m  = std::smatch();
	std::regex_search(m_description, m, re);
	if (!m.empty()) {
		std::string strAddr = m[0].str().substr(1, std::string::npos);	// trim leading "@"
		m_defaultAddress	= strtoul(strAddr.c_str(), nullptr, 16);
	}
}
void Register::setWidth(unsigned int width) { m_width = width; }
void Register::setName(const std::string& name) { m_name = name; }

bool		 Register::isArray() const { return (m_amount > 1) ? true : false; }
unsigned int Register::arraySize() const { return m_amount; }
void		 Register::setArraySize(unsigned int newSize) { m_amount = newSize; }

void Register::asAlignedRegisters(unsigned int align, std::vector<Register>& alignedRegisters) const {
	if (isArray()) {
		if (hasAliases()) {
			if (const auto* anon_alias = alias("")) {
				if (anon_alias->bitfieldCount() == arraySize()) {
					// promote every bitfield to a register
					for (const BitField& bf : *anon_alias) {
						alignedRegisters.emplace_back(m_name + "_" + bf.name(), bf.bitwidth(), bf.isSigned(), bf.description());
					}
				} else {
					throw std::runtime_error("Register::asAlignedRegisters: The number of bitfields has to be the same as the arraySize");
				}
			}
		} else {  // plain array datatype. Each element will be its own register numerated by the index
			for (unsigned int i = 0; i < m_amount; i++) {
				alignedRegisters.emplace_back(m_name + "_" + std::to_string(i), m_width, m_signed, m_description);
			}
		}
	} else {
		Register r = *this;
		r.setWidth(compressedSize());
		alignedRegisters.push_back(r);
	}
}

void to_json(nlohmann::json& j, const Register& reg) {
	nlohmann::json regjson = nlohmann::json{ { "width", reg.width() } };

	if (reg.hasDescription()) regjson.update({ { "description", reg.description() } });

	if (reg.isSigned()) regjson.update({ { "signed", reg.isSigned() } });

	if (reg.hasAliases()) regjson.update({ { "aliases", reg.aliases() } });
	if (reg.defaultValue() != 0) regjson.update({ { "default", reg.defaultValue() } });
	if (reg.hasDefaultAddress()) regjson.update({ { "address", reg.defaultAddress() } });
	j = nlohmann::json{ { reg.name(), regjson } };
}

void from_json(const nlohmann::json& j, Register& reg) {
	for (const auto& e : j.items()) {
		const auto& key = e.key();
		const auto& val = e.value();
		reg.setName(key);
		reg.setWidth(val.at("width").get<unsigned int>());
		try {
			reg.setAliases(val.at("aliases").get<std::vector<Alias>>());
		} catch (const std::exception&) {
		}

		try {
			reg.setDescription(val.at("description").get<std::string>());
		} catch (const std::exception&) {
		}
		try {
			reg.setSigned(val.at("signed").get<bool>());
		} catch (const std::exception&) {
		}
		try {
			reg.setDefaultValue(val.at("default").get<unsigned int>());
		} catch (const std::exception&) {
		}
		try {
			reg.setDefaultAddress(val.at("address").get<unsigned int>());
		} catch (const std::exception&) {
		}
	}
}