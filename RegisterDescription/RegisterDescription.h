#ifndef REGISTER_DESCRIPTION_H
#define REGISTER_DESCRIPTION_H

#include <math.h>
#include <string>
#include <vector>

#include "Alias.h"
#include "BitField.h"
#include "Register.h"
#include "nlohmann/json.hpp"

class RegisterDescription {
private:
	// number of bits for aligning the members
	unsigned int m_align;
	// vector with register name and aliases
	std::vector<Register> m_registers;

	/**
	 * @brief Adjusts the size of the registers according to their true width (counting the bitfields)
	 * This is needed because windows doesn't pack bitfields with different datatypes. According to the 
	 * c++ Spec this is valid. But until I don't know how to disable this via a flag this function should pack it
	 */
	void pack();

public:
	RegisterDescription(unsigned int align = 32) : m_align(align) {
	}

	unsigned int align() const {
		return m_align;
	}
	void setAlign(unsigned int align) {
		m_align = align;
	}

	const std::vector<Register>& registers() const {
		return m_registers;
	}

	void setRegisters(const std::vector<Register>& regs) {
		m_registers = regs;
	}

	void appendRegister(const std::string& name, unsigned int regWidth, bool isSigned, const std::string& description = "") {
		m_registers.push_back(Register(name, regWidth, isSigned, description));
	}
	void appendRegister(const Register& reg) {
		m_registers.push_back(reg);
	}
	void appendAlias(const std::string& alias) {
		m_registers.back().addAlias(alias);
	}
	void appendBitField(const BitField& bf) {
		m_registers.back().appendBitField(bf);
	}

	void makeAligned();
	void insertRegisterAt(unsigned int index, const Register& reg);

	size_t		numberOfRegisters() const;
	std::string toVHDL() const;

	Register* back() {return &m_registers.back();}

	// std::string toJSON() const;

	std::vector<Register>::iterator begin() {
		return m_registers.begin();
	}
	std::vector<Register>::iterator end() {
		return m_registers.end();
	}
	std::vector<Register>::const_iterator begin() const {
		return m_registers.begin();
	}
	std::vector<Register>::const_iterator end() const {
		return m_registers.end();
	}
	std::vector<Register>::reverse_iterator rbegin() {
		return m_registers.rbegin();
	}
	std::vector<Register>::reverse_iterator rend() {
		return m_registers.rend();
	}
	std::vector<Register>::const_reverse_iterator rbegin() const {
		return m_registers.rbegin();
	}
	std::vector<Register>::const_reverse_iterator rend() const {
		return m_registers.rend();
	}
};

extern void to_json(nlohmann::json& j, const RegisterDescription& regDesc);
extern void from_json(const nlohmann::json& j, RegisterDescription& regDesc);

#endif