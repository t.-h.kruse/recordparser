#include "RegisterDescription.h"
#include <iterator>
#include <nlohmann/json.hpp>

std::string RegisterDescription::toVHDL() const {
	std::string out;
	std::string s_regBitWidth = std::to_string(m_align - 1);

	// generate typedef and memory definition
	out += "type register_t is array (0 to " + std::to_string(numberOfRegisters() - 1) + ") of std_logic_vector(" + s_regBitWidth + " downto 0);\n";
	out += "signal regs : register_t := (others => (others => '0'));\n\n";

	// generate register aliases
	unsigned int reg_index = 0;
	for (const auto& r : m_registers) {
		unsigned int num_regs = r.size(m_align);
		out += r.toVHDL(reg_index, m_align);
		if (r.hasDescription()) out += " -- " + r.description();
		out += "\n";
		reg_index += num_regs;
	}

	out += "\n";
	// generate bitfield aliases
	for (const auto& r : m_registers) {
		if (r.hasAliases()) {
			for (const auto& a : r) {
				out += a.toVHDL(r, m_align) + "\n";
			}
		} else {
			std::string bitAlias = "(" + ((r.width() > 1) ? std::to_string(r.width() - 1) + " downto 0" : "0") + ")";
			std::string datatype = (r.width() > 1) ? "std_logic_vector" + bitAlias : "std_logic" + bitAlias;
			out += "alias bf_" + r.name() + "_value : " + datatype + " is reg_" + r.name() + bitAlias + ";";
			if (r.hasDescription()) out += " -- " + r.description();
			out += "\n";
			if (r.width() < m_align) {
				unsigned int msb			 = m_align;
				unsigned int lsb			 = m_align - r.width();
				std::string	 bitAlias_unused = "(" + ((lsb > 1) ? std::to_string(msb - 1) + " downto " + std::to_string(r.width()) : "0") + ")";
				std::string	 datatype_unused =
					(r.width() > 1) ? "std_logic_vector(" + std::to_string(lsb - 1) + " downto " + std::to_string(0) + ")" : "std_logic";

				out += "alias bf_" + r.name() + "_unused : " + datatype_unused + " is reg_" + r.name() + bitAlias_unused + ";\n";
			}
		}
		out += "\n";
	}

	return out;
}

size_t RegisterDescription::numberOfRegisters() const {
	size_t regSize = 0;
	for (const auto& r : m_registers) {
		regSize += r.size(m_align);
	}
	return regSize;
}

void RegisterDescription::insertRegisterAt(unsigned int index, const Register& reg) {
	auto it = m_registers.begin();
	std::advance(it, index);
	m_registers.insert(it, reg);
}

void RegisterDescription::makeAligned() {
	for(auto &r: m_registers){
		for(auto &a : r){
			a.handlePadding();
		}
	}
	
	std::vector<Register> alignedRegs;
	for (const auto& r : m_registers) {
		r.asAlignedRegisters(m_align, alignedRegs);
	}
	m_registers = alignedRegs;

}

void RegisterDescription::pack() {
	for (Register& reg : m_registers) {
		if (reg.hasAliases()) {
			unsigned int maxAliasBitWidth = 0;
			for (const Alias& alias : reg) {
				unsigned int aliasBitwidth = alias.totalBitFieldWidth();
				if (aliasBitwidth > maxAliasBitWidth) maxAliasBitWidth = aliasBitwidth;
			}
			div_t		 res			  = div(maxAliasBitWidth, static_cast<int>(m_align));
			unsigned int newRegisterWidth = res.quot * 32;
			if (res.rem > 0) newRegisterWidth += m_align;
			reg.setWidth(newRegisterWidth);
		}
	}
}

void to_json(nlohmann::json& j, const RegisterDescription& regDesc) {
	j = nlohmann::json{ { "align", regDesc.align() }, { "registers", regDesc.registers() } };
}

void from_json(const nlohmann::json& j, RegisterDescription& regDesc) {
	regDesc.setAlign(j.at("align").get<unsigned int>());
	regDesc.setRegisters(j.at("registers").get<std::vector<Register>>());
}
