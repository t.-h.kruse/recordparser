#include "Alias.h"
#include "Register.h"

std::string Alias::toVHDL(const Register& reg, unsigned int align) const {
	std::string	 out;
	std::string	 aliasName = m_name.empty() ? "" : m_name + "_";
	unsigned int num_regs  = reg.size(align);
	if (num_regs == 1) {
		unsigned int	bitcounter = 0;
		const BitField& last	   = *std::prev(m_bitfields.end());
		for (const BitField& bf : m_bitfields) {
			unsigned int msb	 = bitcounter + bf.bitwidth();
			unsigned int lsb	 = bitcounter;
			std::string bitAlias = bf.bitwidth() > 1 ? "(" + std::to_string(msb - 1) + " downto " + std::to_string(lsb) + ")" : "(" + std::to_string(lsb) + ")";

			out += "alias bf_" + reg.name() + "_" + aliasName + bf.name() + " : " + bf.VHDLdatatype() + " is reg_" + reg.name() + bitAlias + ";";
			if (bf.hasDescription()) out += "-- " + bf.description();
			bitcounter = msb;
			if (&bf != &last) out += "\n";
		}
		if (bitcounter < align && m_name.empty()) {	 // create unused bitfield only for the the anonymous struct
			out += "\n" + unusedBitfieldString(reg, align, bitcounter);
		}
	} else {
		if (m_bitfields.size() == num_regs) {
			std::string	 aliasName	= m_name.empty() ? "" : m_name + "_";
			unsigned int bitcounter = 0;
			for (unsigned int i = 0; i < m_bitfields.size(); i++) {
				const BitField& bf	= m_bitfields[i];
				unsigned int	msb = bitcounter + bf.bitwidth();
				unsigned int	lsb = bitcounter;
				std::string		bitAlias =
					bf.bitwidth() > 1 ? "(" + std::to_string(msb - 1) + " downto " + std::to_string(lsb) + ")" : "(" + std::to_string(lsb) + ")";
				out += "alias bf_" + reg.name() + "_" + aliasName + bf.name() + " : " + bf.VHDLdatatype() + " is reg_" + reg.name() + "_" + bf.name() +
					   bitAlias + ";";
				if (bf.hasDescription()) out += " -- " + bf.description();

				if (i < m_bitfields.size() - 1) {
					out += "\n";
					const BitField& nextbf = m_bitfields[i + 1];
					if (msb + nextbf.bitwidth() > align) {
						// on overflow
						out += unusedBitfieldString(reg, align, msb, bf.name()) + "\n";
						bitcounter = 0;
					} else
						bitcounter = msb;
				}
			}
		} else {
			out += "-- unimplemented structure for reg_" + reg.name() + "_" + m_name + "\n";
			out += "-- The number of bitfields has to be the same as the regs.size";
		}
	}
	return out;
}

std::string Alias::unusedBitfieldString(const Register& reg, unsigned int align, unsigned int current_bitcounter, const std::string& suffix) const {
	unsigned int msb			 = align;
	unsigned int lsb			 = current_bitcounter;
	unsigned int unused_width = msb - lsb;
	std::string	 bitAlias_unused = "(" + ((unused_width > 1) ? std::to_string(msb - 1) + " downto " + std::to_string(lsb) : "0") + ")";
	std::string	 datatype_unused = (unused_width > 1) ? "std_logic_vector(" + std::to_string(msb - lsb - 1) + " downto " + std::to_string(0) + ")" : "std_logic";
	std::string	 unused_bits	 = (unused_width > 1) ? std::to_string(msb - 1) + "_to_" + std::to_string(lsb) : std::to_string(lsb);

	return "alias bf_" + reg.name() + (suffix.empty() ? "" : "_" + suffix) + "_unused_" + unused_bits + " : " + datatype_unused + " is reg_" + reg.name() +
		   bitAlias_unused + ";";
}

Alias::Alias(const std::string& name) : m_name(name) {
}
Alias::Alias() {
}

unsigned int Alias::bitfieldCount() const {
	return static_cast<unsigned int>(m_bitfields.size());
}

std::string Alias::name() const {
	return m_name;
}

const std::vector<BitField>& Alias::bitfields() const {
	return m_bitfields;
}

void Alias::appendBitField(const BitField& bf) {
	m_bitfields.push_back(bf);
}

const BitField& Alias::back() const {
	return m_bitfields.back();
}

unsigned int Alias::defaultValue() const {
	unsigned int val		= 0;
	unsigned int bitCounter = 0;
	for (const auto& bf : m_bitfields) {
		val |= bf.defaultValue() << bitCounter;
		bitCounter += bf.bitwidth();
	}
	return val;
}

unsigned int Alias::mask() const {
	unsigned int mask		= 0;
	unsigned int bitcounter = 0;
	for (const BitField& bf : m_bitfields) {
		mask |= bf.mask() << bitcounter;
		bitcounter += bf.bitwidth();
	}
	return mask;
}

unsigned int Alias::numberOfUsedBits() const {
	unsigned int numBits = 0;
	for (const BitField& bf : m_bitfields) {
		numBits += bf.bitwidth();
	}
	return numBits;
}

unsigned int Alias::totalBitFieldWidth() const {
	unsigned int cnt = 0;
	for (const BitField& bf : m_bitfields) {
		cnt += bf.bitwidth();
	}
	return cnt;
}

void Alias::alignBitfields() {
	unsigned int					currentBitfieldIndex = 0;
	std::vector<BitField>::iterator it;
	for (it = m_bitfields.begin(); it != m_bitfields.end(); ++it) {
		if (!it->name().empty()) {
			// if the name of the bitfield startswith unused/reserved then the bitmask for it is 0x0
			std::regex	re("(unused|reserved)", std::regex_constants::icase);
			std::smatch m;
			std::string name = it->name();
			std::regex_search(name, m, re);
			if (!m.empty()){
				if(it->bitwidth() > 1)
					it->setName(it->name() + "_" + std::to_string(currentBitfieldIndex) + "_to_" + std::to_string(currentBitfieldIndex + it->bitwidth() - 1));
				else
					it->setName(it->name() + "_" + std::to_string(currentBitfieldIndex));
			}

			currentBitfieldIndex += it->bitwidth();
		} else {
			if (it->bitwidth() == 0) {	// align bitfield to next byte
				std::vector<BitField>::const_iterator next_bf = std::next(it);
				if (next_bf != m_bitfields.end()) { // if a padding bitfield is between bitfields
					// insert padding to next byte
					unsigned int reservedBitWidth = 8 - currentBitfieldIndex % 8;
					if (reservedBitWidth > 0) {
						it->setBitwidth(reservedBitWidth);

						if(it->bitwidth() > 1)
							it->setName("unused_" + std::to_string(currentBitfieldIndex) + "_to_" + std::to_string(currentBitfieldIndex + reservedBitWidth - 1));
						else
							it->setName("unused_" + std::to_string(currentBitfieldIndex));
						
						currentBitfieldIndex += reservedBitWidth;
					}  // else if 0 bits then go the the next bitfield
				}	   // else ignore padding for last bitfield
			} else {   // anonymous bitfield with specified bitwidth only get a name
				if(it->bitwidth() > 1)
					it->setName("unused_" + std::to_string(currentBitfieldIndex) + "_to_" + std::to_string(currentBitfieldIndex + it->bitwidth() - 1));
				else
					it->setName("unused_" + std::to_string(currentBitfieldIndex));
				
				currentBitfieldIndex += it->bitwidth();
			}
		}
	}
}

void Alias::handlePadding(){
	// remove all trailing bitfields that have width 0
	if(m_bitfields.size() > 0){
		BitField &last_bf = m_bitfields.back();
		while(last_bf.bitwidth() == 0 || last_bf.name().empty()){
			m_bitfields.pop_back();
			last_bf = m_bitfields.back();
		}
	}

	// align all bitfields that have width 0
	unsigned int bitcounter = 0;
	for(auto& bf : m_bitfields){
		if(bf.bitwidth() > 0){
			bitcounter += bf.bitwidth();
		}else{
			div_t res = div(bitcounter, 8);
			unsigned int paddingBits = 8 - res.rem;
			bf.setBitwidth(paddingBits);
		}
	}

	// name all unnamed bitfields
	bitcounter = 0;
	for(auto& bf : m_bitfields){
		unsigned int bitwidth = bf.bitwidth();
		if(bf.name().empty()){
			if(bitwidth > 1)
				bf.setName("unused_" + std::to_string(bitcounter) + "_to_" + std::to_string(bitcounter + bf.bitwidth() - 1));
			else
				bf.setName("unused_" + std::to_string(bitcounter));
		}
		bitcounter += bf.bitwidth();
	}
}

std::vector<BitField>::iterator Alias::begin() {
	return m_bitfields.begin();
}
std::vector<BitField>::iterator Alias::end() {
	return m_bitfields.end();
}
std::vector<BitField>::const_iterator Alias::begin() const {
	return m_bitfields.begin();
}
std::vector<BitField>::const_iterator Alias::end() const {
	return m_bitfields.end();
}

std::vector<BitField>::reverse_iterator Alias::rbegin() {
	return m_bitfields.rbegin();
}
std::vector<BitField>::reverse_iterator Alias::rend() {
	return m_bitfields.rend();
}
std::vector<BitField>::const_reverse_iterator Alias::rbegin() const {
	return m_bitfields.rbegin();
}
std::vector<BitField>::const_reverse_iterator Alias::rend() const {
	return m_bitfields.rend();
}

void Alias::setName(const std::string& name) {
	m_name = name;
}
void Alias::setBitfields(const std::vector<BitField>& bitfields) {
	m_bitfields = bitfields;
}

void to_json(nlohmann::json& j, const Alias& alias) {
	j = nlohmann::json{ { alias.name(), alias.bitfields() } };
}

void from_json(const nlohmann::json& j, Alias& alias) {
	for (const auto& e : j.items()) {
		const auto& key	  = e.key();
		const auto& value = e.value();
		alias.setName(key);
		alias.setBitfields(value);
	}
}