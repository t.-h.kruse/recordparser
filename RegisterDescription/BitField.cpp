#include "BitField.h"

std::string BitField::toVHDL() const {
	std::string out = "not implemented yet";
	return out;
}

BitField::BitField() : m_bitwidth(0), m_defaultValue(0), m_signed(false) {}

bool BitField::hasDescription() const { return !m_description.empty(); }

void to_json(nlohmann::json& j, const BitField& bf) {
	nlohmann::json bitjson = nlohmann::json{ { "bitwidth", bf.bitwidth() } };

	if (bf.hasDescription()) bitjson.update({ { "description", bf.description() } });
	if (bf.isSigned()) bitjson.update({ { "signed", bf.isSigned() } });
	if (bf.defaultValue() != 0) bitjson.update({ { "default", bf.defaultValue() } });
	j = nlohmann::json{ { bf.name(), bitjson } };
}

void from_json(const nlohmann::json& j, BitField& bf) {
	for (const auto& e : j.items()) {
		const auto& key = e.key();
		const auto& val = e.value();
		bf.setName(key);
		bf.setBitwidth(val.at("bitwidth").get<unsigned int>());
		try {
			bf.setDescription(val.at("description").get<std::string>());
		} catch (const std::exception&) {
		}
		try {
			bf.setSigned(val.at("signed").get<bool>());
		} catch (const std::exception&) {
		}
		try {
			auto v = val.at("default").get<unsigned int>();
			bf.setDefaultValue(v);
		} catch (const std::exception&) {
		}
	}
}

unsigned int BitField::defaultValue() const {
	unsigned int mask = (1 << m_bitwidth) - 1;
	return m_defaultValue & mask;
}
void BitField::setDefaultValue(unsigned int newValue) { m_defaultValue = newValue; }

unsigned int BitField::mask() const {
	// if the name of the bitfield is unused/reserved then the bitmask for it is 0x0
	std::regex	re("(unused|reserved)", std::regex_constants::icase);
	std::smatch m;
	std::regex_search(m_name, m, re);
	if (!m.empty())
		return 0;
	else
		return static_cast<unsigned int>((static_cast<uint64_t>(1) << m_bitwidth) - 1);
}