#ifndef ALIAS_H_
#define ALIAS_H_

#include <string>
#include <vector>
#include "nlohmann/json.hpp"

#include "BitField.h"
class Register;

class Alias {
private:
	std::string			  m_name;
	std::vector<BitField> m_bitfields;

public:
	Alias(const std::string& name);
	Alias();

	std::string					 name() const;
	const std::vector<BitField>& bitfields() const;

	void		 setName(const std::string& name);
	void		 setBitfields(const std::vector<BitField>& bitfields);
	unsigned int bitfieldCount() const;

	/**
	 * @brief Calculates the number of bits used by this alias
	 * 
	 * @return unsigned int Total number of used bits in this alias
	 */
	unsigned int totalBitFieldWidth() const;
	void alignBitfields();
	void handlePadding();

	unsigned int defaultValue() const;
	unsigned int mask() const;

	void appendBitField(const BitField& bf);

	std::string toVHDL(const Register& reg, unsigned int align) const;
	std::string unusedBitfieldString(const Register& reg, unsigned int align, unsigned int current_bitcounter, const std::string& suffix = "") const;

	unsigned int numberOfUsedBits() const;

	const BitField& back() const;

	std::vector<BitField>::iterator				  begin();
	std::vector<BitField>::iterator				  end();
	std::vector<BitField>::const_iterator		  begin() const;
	std::vector<BitField>::const_iterator		  end() const;
	std::vector<BitField>::reverse_iterator		  rbegin();
	std::vector<BitField>::reverse_iterator		  rend();
	std::vector<BitField>::const_reverse_iterator rbegin() const;
	std::vector<BitField>::const_reverse_iterator rend() const;
};

extern void to_json(nlohmann::json& j, const Alias& alias);
extern void from_json(const nlohmann::json& j, Alias& alias);

#endif