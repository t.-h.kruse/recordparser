#ifndef BITFIELD_H_
#define BITFIELD_H_

#include <stdlib.h>
#include <iostream>
#include <regex>
#include <string>
#include "nlohmann/json.hpp"

class BitField {
private:
	unsigned int m_bitwidth;
	unsigned int m_defaultValue;
	bool		 m_signed;
	std::string	 m_name;
	std::string	 m_description;

public:
	BitField(const std::string& name, unsigned int bitwidth, bool isSigned, const std::string& description = "")
		: m_bitwidth(bitwidth), m_defaultValue(0), m_signed(isSigned), m_name(name), m_description(description) {
		std::regex	re("default\\s*[:=]\\s*([[:xdigit:]]|-?)x?[[:xdigit:]]*", std::regex_constants::icase);
		std::smatch m;
		std::regex_search(m_description, m, re);
		if (!m.empty()) {
			std::string str		  = m[0].str();
			size_t		splitHere = str.find_last_of(" =:");
			if (splitHere != std::string::npos) {
				std::string defaultValueStr = str.substr(splitHere + 1, std::string::npos);
				m_defaultValue				= strtoul(defaultValueStr.c_str(), nullptr, 0);
			}
		}
	}
	BitField();

	std::string name() const {
		return m_name;
	}
	void setName(const std::string& newName) {
		m_name = newName;
	}
	unsigned int bitwidth() const {
		return m_bitwidth;
	}
	void setBitwidth(unsigned int newBitwidth) {
		m_bitwidth = newBitwidth;
	}

	bool isSigned() const {
		return m_signed;
	}

	void setSigned(bool s) {
		m_signed = s;
	}
	unsigned int defaultValue() const;
	void		 setDefaultValue(unsigned int newValue);

	std::string description() const {
		return m_description;
	}
	void setDescription(const std::string& newDescription) {
		m_description = newDescription;
	}
	unsigned int mask() const;

	bool hasDescription() const;

	std::string toVHDL() const;

	std::string VHDLdatatype() const {
		std::string out;
		if (m_bitwidth > 1)
			out += "std_logic_vector(" + std::to_string(m_bitwidth - 1) + " downto 0)";
		else
			out += "std_logic";
		return out;
	}
};

extern void to_json(nlohmann::json& j, const BitField& bf);
extern void from_json(const nlohmann::json& j, BitField& bf);

#endif