#include "vhdlgen.h"
#include <bitset>
#include <cmath>
#include <regex>

template <typename I>
std::string n2hexstr(I w, size_t hex_len = sizeof(I) << 1) {
	static const char* digits = "0123456789ABCDEF";
	std::string		   rc(hex_len, '0');
	for (size_t i = 0, j = (hex_len - 1) * 4; i < hex_len; ++i, j -= 4) rc[i] = digits[(w >> j) & 0x0f];
	return rc;
}

std::string VHDLGen::generateAvalonTopLevelWrapper(const std::shared_ptr<RegisterDescription> registerDescription, const std::string& module_name) {
	std::string out = "-- altera vhdl_input_version vhdl_2008\n";
	out += libs("ieee", { "std_logic_1164", "numeric_std" }) + "\n";
	out += entity(module_name,
				  { 
					{ "clk", 1 },
					{ "reset_n", 1 },
					{ "avs_s0_read", 1 },
					{ "avs_s0_write", 1 },
					{ "avs_s0_address", static_cast<unsigned int>(ceil(log(registerDescription->numberOfRegisters()) / log(2))) },
					{ "avs_s0_writedata", 32 } 
				},
				{
					{ "avs_s0_readdata", 32 } 
				}
				) +
		   "\n";
	const std::string& out_write_proc = avalon_write_proc("avalon_write_proc", EDGE_Rising, *registerDescription.get());
	const std::string& out_read_proc  = avalon_read_proc("avalon_read_proc", *registerDescription.get());
	const std::string& out_proc		  = out_read_proc + "\n" + out_write_proc + "\n";
	out += arch("rtl", module_name, registerDescription->toVHDL(), out_proc);
	return out;
}

std::string VHDLGen::libs(const std::string& lib, const std::vector<std::string>& packages) {
	std::string out = "library " + lib + ";\n";
	for (const auto& p : packages) {
		out += "use " + lib + "." + p + ".all;\n";
	}
	return out;
}

std::string VHDLGen::entity(const std::string& name, const std::vector<std::pair<std::string, unsigned int>>& portListInput,
							const std::vector<std::pair<std::string, unsigned int>>& portListOutput) {
	std::string entity_def = "entity " + name + " is\n\tport(\n";
	if (!portListInput.empty()) {
		for (int i = 0; i < portListInput.size() - 1; i++) {
			const auto& signal	 = portListInput[i];
			std::string datatype = "std_logic";
			if (signal.second > 1) {
				datatype += "_vector(" + std::to_string(signal.second - 1) + " downto 0)";
			}
			entity_def += std::string(2, '\t') + signal.first + " : in " + datatype + ";\n";
		}
		const auto& lastInSignal = portListInput[portListInput.size() - 1];
		std::string datatype	 = "std_logic";
		if (lastInSignal.second > 1) {
			datatype += "_vector(" + std::to_string(lastInSignal.second - 1) + " downto 0)";
		}
		entity_def += std::string(2, '\t') + lastInSignal.first + " : in " + datatype;
		if (!portListOutput.empty())
			entity_def += ";\n";
		else
			entity_def += "\n";
	}
	if (!portListOutput.empty()) {
		for (int i = 0; i < portListOutput.size() - 1; i++) {
			const auto& signal	 = portListOutput[i];
			std::string datatype = "std_logic";
			if (signal.second > 1) {
				datatype += "_vector(" + std::to_string(signal.second - 1) + " downto 0)";
			}
			entity_def += std::string(2, '\t') + signal.first + " : out " + datatype + ";\n";
		}
		const auto& lastOutSignal = portListOutput[portListOutput.size() - 1];
		std::string datatype	  = "std_logic";
		if (lastOutSignal.second > 1) {
			datatype += "_vector(" + std::to_string(lastOutSignal.second - 1) + " downto 0)";
		}
		entity_def += std::string(2, '\t') + lastOutSignal.first + " : out " + datatype + "\n";
	}

	entity_def += "\t);\nend entity " + name + ";\n";
	return entity_def;
}

std::string VHDLGen::arch(const std::string& arch, const std::string& entity_name, const std::string& entry, const std::string& impl) {
	std::string impl_indented  = impl.empty() ? "" : "\t" + std::regex_replace(impl, std::regex("\n"), "\n\t") + "\n";
	std::string entry_indented = entry.empty() ? "" : "\t" + std::regex_replace(entry, std::regex("\n"), "\n\t") + "\n";
	std::string out			   = "architecture " + arch + " of " + entity_name + " is\n";
	out += entry_indented + "begin\n" + impl_indented + "end architecture " + arch + ";";
	return out;
}

std::string VHDLGen::avalon_write_proc(const std::string& name, VHDLGen::edge_t edge, const RegisterDescription& regs) {
	std::string	 out;
	unsigned int indent = 0;
	out += "-- remove read only registers!!\n";
	out += name + " : process (clk)\nbegin\n";
	indent++;
	out += std::string(indent, '\t') + "if " + (edge == VHDLGen::edge_t::EDGE_Rising ? "rising_edge(clk)" : "falling_edge(clk") + " then\n";
	indent++;
	out += std::string(indent, '\t') + "if reset_n = '0' then\n";
	indent++;
	for (unsigned int i = 0; i < regs.numberOfRegisters(); i++) {
		out += std::string(indent, '\t') + "regs(" + std::to_string(i) + ") <= " + "(others => '0')" + ";\n";
	}
	indent--;
	out += std::string(indent, '\t') + "else\n";
	indent++;
	out += std::string(indent, '\t') + "if avs_s0_write = '1' then\n";
	indent++;
	out += std::string(indent, '\t') + "case avs_s0_address is\n";
	indent++;
	const unsigned int numRegs		  = static_cast<unsigned int>(regs.numberOfRegisters());
	const unsigned int num_digits	  = (unsigned int)ceil(log(regs.numberOfRegisters()) / log(2));
	const std::string  bitQuotingChar = (num_digits > 1) ? "\"" : "'";

	for (unsigned int i = 0; i < numRegs; i++) {
		std::string bitStr		  = std::bitset<32>(i).to_string();
		std::string bitStrTrimmed = bitStr.substr(32 - num_digits, num_digits);
		out += std::string(indent, '\t') + "when " + bitQuotingChar + bitStrTrimmed + bitQuotingChar + " => regs(" + std::to_string(i) +
			   ") <= avs_s0_writedata;\n";
	}
	out += std::string(indent, '\t') + "when others =>\n";
	indent++;
	for (unsigned int i = 0; i < numRegs; i++) {
		out += std::string(indent, '\t') + "regs(" + std::to_string(i) + ") <= " + "regs(" + std::to_string(i) + ")" + ";\n";
	}
	indent--;
	indent--;
	out += std::string(indent, '\t') + "end case;\n";
	indent--;
	out += std::string(indent, '\t') + "end if;\n";
	indent--;
	out += std::string(indent, '\t') + "end if;\n";
	indent--;
	out += std::string(indent, '\t') + "end if;\n";
	indent--;
	out += std::string(indent, '\t') + "end process " + name + ";\n";

	return out;
}

std::string VHDLGen::avalon_read_proc(const std::string& name, const RegisterDescription& regs) {
	std::string	 out;
	unsigned int indent = 0;
	out += name + " : process (all)\nbegin\n";
	indent++;
	out += std::string(indent, '\t') + "if avs_s0_read = '1' then\n";
	indent++;
	out += std::string(indent, '\t') + "case avs_s0_address is\n";
	indent++;
	unsigned int	   numRegs		  = static_cast<unsigned int>(regs.numberOfRegisters());
	const unsigned int num_digits	  = (unsigned int)ceil(log(regs.numberOfRegisters()) / log(2));
	const std::string  bitQuotingChar = (num_digits > 1) ? "\"" : "'";
	for (unsigned int i = 0; i < regs.numberOfRegisters(); i++) {
		std::string bitStr		  = std::bitset<32>(i).to_string();
		std::string bitStrTrimmed = bitStr.substr(32 - num_digits, num_digits);
		out +=
			std::string(indent, '\t') + "when " + bitQuotingChar + bitStrTrimmed + bitQuotingChar + " => avs_s0_readdata <= regs(" + std::to_string(i) + ");\n";
	}
	out += std::string(indent, '\t') + "when others => avs_s0_readdata <= (others => '0');\n";
	indent--;
	out += std::string(indent, '\t') + "end case;\n";
	indent--;
	out += std::string(indent, '\t') + "else\n";
	indent++;
	out += std::string(indent, '\t') + "avs_s0_readdata <= (others => '0');\n";
	indent--;
	out += std::string(indent, '\t') + "end if;\n";

	indent--;
	out += std::string(indent, '\t') + "end process " + name + ";\n";
	return out;
}