#include <iostream>
#include <memory>
#include "vhdl/Alias.h"
#include "vhdl/Signal.h"
#include "vhdl/Types.h"
#include "vhdl/Wire.h"

int main(int argc, char const* argv[]) {
	using namespace std;
	using namespace vhdl;

	std::unique_ptr<Type> type(static_cast<Type*>(new StdLogicVectorType('1', 16)));
	Signal				  sig("s_tmp", std::move(type));

	Alias alias_hb("bf_hb", static_cast<Wire*>(&sig), 8, 16);
	Alias alias_hh("bf_hh", static_cast<Wire*>(&alias_hb), 4, 8);

	cout << sig << endl;
	cout << alias_hb << endl;
	cout << alias_hh << endl << endl;
	cout << *alias_hb.aliasedWire() << endl;
	cout << *alias_hh.aliasedWire() << endl;
	return 0;
}
