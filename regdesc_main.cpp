#include <fstream>
#include <iostream>
#include "RegisterDescription/RegisterDescription.h"
#include "nlohmann/json.hpp"

int main(int argc, char* argv[]) {
	using namespace std;
	nlohmann::json j;
	// ifstream	   is("/Users/tim/Documents/Programming/llvm/recordparser/example/example.json");
	ifstream	   is("/Users/tim/Applications/arm-dev/portgroup.json");
	is >> j;
	RegisterDescription regDesc = j.get<RegisterDescription>();
	regDesc.makeAligned();
	for (const auto& reg : regDesc) {
		unsigned int regmask = reg.mask();
		for (const Alias& alias : reg) {
			unsigned int aliasmask = alias.mask();
			for (const BitField& bf : alias) {
				unsigned int bfmask = bf.mask();
				cout << std::hex << reg.name() + ": " << regmask << ", " + alias.name() + ": " << aliasmask << ", " + bf.name() + ": " << bfmask << ", width: (0x" << bf.bitwidth() << ")" << endl;
			}
		}
	}
	cout << "done" << endl;
	return 0;
}