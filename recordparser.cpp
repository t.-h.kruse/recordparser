#include <string>
#include <vector>
#include "RegisterDescription/RegisterDescription.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/JSONCompilationDatabase.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/ADT/Twine.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/FileSystem.h"
#include "vhdlgen.h"

using namespace clang::ast_matchers;
using namespace clang::tooling;
using namespace clang;
using namespace llvm;

/**
 * @brief Callback class that is called when the AST Matcher finds a match/record
 * Every match walks recursively through the members/fields and parses them as well.
 * Therefore, the MatchFinder in the main function should be configured to find
 * something very specific. In my case it's a uniquely named typedef struct
 */
class RecordPrinter : public MatchFinder::MatchCallback {
	bool								 m_verbose = false;		/**< if true the findings are printed to stdout*/
	std::shared_ptr<RegisterDescription> m_registerDescription; /**< Data structure to store the findings. Initialized only on a match.*/

public:
	/**
	 * @brief Construct a new Record Printer object
	 *
	 * @param verbose If true print the findings to stdout
	 */
	RecordPrinter(bool verbose) : m_verbose(verbose) {
	}
	/**
	 * @brief Getter for the data structure
	 *
	 * @return std::shared_ptr<RegisterDescription> returns the datastructure.
	 */
	std::shared_ptr<RegisterDescription> registerDescription() {
		return m_registerDescription;
	}

	/**
	 * @brief Function that is run on a match
	 *
	 * @param Result The AST Member that matched
	 */
	virtual void run(const MatchFinder::MatchResult &Result) {
		m_registerDescription = std::make_shared<RegisterDescription>();
		ASTContext *Context	  = Result.Context;
		if (const TypedefDecl *T = Result.Nodes.getNodeAs<clang::TypedefDecl>("toplevel")) {
			FullSourceLoc FullLocation = Context->getFullLoc(T->getBeginLoc());
			if (FullLocation.isValid()) {
				// get the underlying struct that is typedef'd
				clang::RecordDecl *topLevelRecord = T->getUnderlyingType().getCanonicalType().getTypePtr()->getAsRecordDecl();

				if (m_verbose)
					llvm::outs() << "Found typedef \"" << T->getNameAsString() << "\" at "
								 << FullLocation.getManager().getFilename(FullLocation.getSpellingLoc()) << " " << FullLocation.getSpellingLineNumber() << ":"
								 << FullLocation.getSpellingColumnNumber() << "\n";

				auto typeinfo = Context->getTypeInfo(topLevelRecord->getTypeForDecl());
				m_registerDescription->setAlign(typeinfo.Align);

				// start walking through the record fields recursively
				recursivelyTraverseFieldsOfRecord(Context, topLevelRecord, m_verbose);
			}
		} else {
			llvm::outs() << "something else\n";
		}
	}

private:
	/**
	 * @brief Recursively walks through the fields of a record, to extract the information
	 *
	 * @param context
	 * @param record Top-Level record to start with
	 * @param print Print findings to stdout
	 * @param level Specifies the times a field references another field.
	 */
	void recursivelyTraverseFieldsOfRecord(ASTContext *context, const clang::RecordDecl *record, bool print = true, unsigned int level = 0) {
		for (const auto *field : record->fields()) {
			const clang::Type* fieldtype = field->getType().getTypePtr();
			std::string		fieldname = field->getNameAsString();

			clang::TypeInfo typeinfo	  = context->getTypeInfo(field->getType());
			bool typeIsSigned = fieldtype->hasSignedIntegerRepresentation();

			const clang::RawComment *rc = context->getRawCommentForDeclNoCache(field);

			if (fieldtype->isRecordType()) {
				auto recordfield = fieldtype->getAsRecordDecl();

				std::string recordTypeName;
				if (recordfield->isUnion())
					recordTypeName = " union";
				else if (recordfield->isStruct())
					recordTypeName = " struct";

				if (level == 0) {
					m_registerDescription->appendRegister(fieldname, static_cast<unsigned int>(typeinfo.Width), typeIsSigned, rc ? rc->getBriefText(*context) : "");
				} else {
					m_registerDescription->appendAlias(fieldname);
				}

				if (print)
					llvm::outs() << std::string(level * 2, ' ') << "`-" << (fieldname.empty() ? "anonymous" + recordTypeName : fieldname) << ": "
								 << recordfield->getNameAsString() << "\n";
				recursivelyTraverseFieldsOfRecord(context, recordfield, print, level + 1);

			} else if (fieldtype->isIntegerType()) {
				clang::QualType builtintype = fieldtype->getCanonicalTypeInternal();
				clang::QualType typedeftype = field->getType();

				if (level == 0) {
					m_registerDescription->appendRegister(	fieldname, 
															field->isBitField() ? static_cast<unsigned int>(field->getBitWidthValue(*context)) : static_cast<unsigned int>(typeinfo.Width), 
															typeIsSigned,
														  	rc ? rc->getBriefText(*context) : ""
														);
				} else if (level == 2) {
					if (field->isBitField())
						m_registerDescription->appendBitField(
							BitField(fieldname, field->getBitWidthValue(*context), typeIsSigned, rc ? rc->getBriefText(*context) : ""));
				}

				if (print)
					llvm::outs() << std::string(level * 2, ' ') << "`-" << fieldname << ": " << typedeftype.getAsString() << " (" << builtintype.getAsString()
								 << ")" << (field->isBitField() ? ", bits: " + std::to_string(field->getBitWidthValue(*context)) : "") << "\n";
			} else if (fieldtype->isArrayType()) {
				clang::QualType a = fieldtype->getCanonicalTypeInternal();								 // unsigned int [2]
				// clang::QualType b = fieldtype->getAsArrayTypeUnsafe()->getElementType();	 // uint32_t
				const clang::ConstantArrayType* c = context->getAsConstantArrayType(a);
				clang::QualType arrayType = c->getElementType();
				APInt arraySize = c->getSize();	
				uint64_t typesize = context->getTypeSize(arrayType);

				const clang::Type* baseType = arrayType.getUnqualifiedType().getTypePtr();

				if (print){
					llvm::outs() << std::string(level * 2, ' ') << "`-" << fieldname << ": (" << arrayType.getAsString() << "(" << typesize << " bits))[" << arraySize << "]\n";
				}
				if (level == 0) {
					Register newReg(fieldname, static_cast<unsigned int>(typesize), typeIsSigned, rc ? rc->getBriefText(*context) : "");
					newReg.setArraySize(static_cast<unsigned int>(arraySize.getZExtValue()));
					m_registerDescription->appendRegister(newReg);
					
					if(baseType->isRecordType()){
						recursivelyTraverseFieldsOfRecord(context, baseType->getAsRecordDecl(), print, level+1);
					}

					// for(uint64_t i = 1; i < arraySize.getZExtValue(); i++){
					// 	Register *previousReg = m_registerDescription->back();
					// 	m_registerDescription->appendRegister(*previousReg);
					// 	previousReg->setName(previousReg->name() + "[" + std::to_string(i-1) + "]");
					// }
					// Register *lastReg = m_registerDescription->back();
					// lastReg->setName(lastReg->name() + "[" + std::to_string(arraySize.getZExtValue() - 1) + "]");
				}else if(level == 1){
					Register *r = m_registerDescription->back();
					r->setArraySize(static_cast<unsigned int>(arraySize.getZExtValue()));
				}
				
			} else {
				if (print) llvm::outs() << "??\n";
			}
		}
	}
};

// Command line options
enum OutputFormat { vhdl, json, none };
static llvm::cl::OptionCategory MyToolCategory("recordparser options");
static cl::opt<std::string>		recordName("m", cl::desc("Specify top-level record/struct"), cl::cat(MyToolCategory), cl::Required, cl::value_desc("record"));
static cl::opt<std::string>		outputFile("o", cl::desc("Specify output filename"), cl::cat(MyToolCategory), cl::init("-"), cl::value_desc("filename"));
static cl::opt<bool>			verbose("v", cl::desc("Print all findings"), cl::cat(MyToolCategory), cl::init(false));
static cl::opt<OutputFormat>	outputFileFormat("f", cl::desc("Choose output format"), cl::cat(MyToolCategory), cl::init(vhdl), cl::values(clEnumVal(vhdl, "Emit VHDL code"), clEnumVal(json, "Emit a JSON"), clEnumVal(none, "Skip code generation")));

/**
 * @brief Removes a given suffix from the passed string
 *
 * @param str input string
 * @param suffix suffix that should be removed
 * @return std::string string with the suffix removed or the original string if the suffix is not there
 */
std::string removedSuffix(const std::string &str, const std::string &suffix) {
	size_t lastindex = str.find_last_of(suffix);
	if (lastindex < std::string::npos) {
		return str.substr(0, lastindex - 1);
	}
	return str;
}

int main(int argc, char const *argv[]) {
	CommonOptionsParser OptionsParser(argc, argv, MyToolCategory);
	ClangTool			Tool(OptionsParser.getCompilations(), OptionsParser.getSourcePathList());

	RecordPrinter Printer(verbose.getValue());
	MatchFinder	  Finder;

	// Create a matcher that matches all typedef'd records (structs and unions) AND the record with a name (from cmd-line)
	DeclarationMatcher m_name		  = typedefDecl(hasName(recordName.getValue())).bind("toplevel_typedef_name");
	auto			   m_typdefrecord = typedefDecl(hasType(elaboratedType(namesType(recordType())))).bind("toplevel_typedef_record");
	auto			   matcher		  = typedefDecl(allOf(m_name, m_typdefrecord)).bind("toplevel");

	// install and run the tool/llvm
	Finder.addMatcher(matcher, &Printer);
	auto factory = newFrontendActionFactory(&Finder);
	int	 result	 = Tool.run(factory.get());

	if(outputFileFormat.getValue() == OutputFormat::none) return result;
	if (result == 0) {
		if (auto regDesc = Printer.registerDescription()) {
			regDesc->makeAligned();
			std::string fileEnding = ".vhd";
			switch (outputFileFormat.getValue()) {
				case json: fileEnding = ".json"; break;
				case vhdl: fileEnding = ".vhd"; break;
				default: fileEnding = ".txt"; break;
			}

			std::string vhdl_module_name = removedSuffix(recordName.getValue(), "_t");
			if (outputFile.getValue() == "-") {	 // if no outputfile name is specified on the cmd-line, then print to stdout
				switch (outputFileFormat.getValue()) {
					case json: {
						nlohmann::json j = *regDesc.get();
						llvm::outs() << j.dump(4) << "\n";
						break;
					}
					default: llvm::outs() << VHDLGen::generateAvalonTopLevelWrapper(regDesc, vhdl_module_name); break;
				}
			} else {  // print into file
				// open file
				std::error_code		 ec;
				llvm::raw_fd_ostream fs(outputFile.getValue(), ec, sys::fs::CreationDisposition::CD_CreateAlways, sys::fs::FileAccess::FA_Write,
										sys::fs::OpenFlags::OF_Text);

				switch (outputFileFormat.getValue()) {
					case json: {
						nlohmann::json j = *regDesc.get();
						fs << j.dump(4) << "\n";
						break;
					}
					default: fs << VHDLGen::generateAvalonTopLevelWrapper(regDesc, vhdl_module_name + "_avalon"); break;
				}
				
				fs.close();
			}

		} else {
			llvm::outs() << "Record '" << recordName.getValue() << "' not found\n";
		}
	}

	return result;
}
