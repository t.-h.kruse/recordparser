#ifndef VHDL_GEN_H_
#define VHDL_GEN_H_

#include <memory>
#include <string>
#include <vector>
#include "RegisterDescription/RegisterDescription.h"
// #include "llvm/Support/FileSystem.h"
// #include "llvm/Support/raw_os_ostream.h"

class VHDLGen {
public:
	typedef enum { PROC_Sync, PROC_Async } proc_t;
	typedef enum { EDGE_Rising, EDGE_Falling } edge_t;

	static std::string generateAvalonTopLevelWrapper(const std::shared_ptr<RegisterDescription> registerDescription, const std::string& module_name);

	static std::string libs(const std::string& lib, const std::vector<std::string>& usedLibs);
	static std::string entity(const std::string& name, const std::vector<std::pair<std::string, unsigned int>>& portListInput,
							  const std::vector<std::pair<std::string, unsigned int>>& portListOutput);
	static std::string arch(const std::string& arch, const std::string& entity_name, const std::string& entry, const std::string& impl);
	static std::string avalon_write_proc(const std::string& name, VHDLGen::edge_t edge, const RegisterDescription& regs);
	static std::string avalon_read_proc(const std::string& name, const RegisterDescription& regs);
};

#endif