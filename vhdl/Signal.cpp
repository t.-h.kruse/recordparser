#include "Signal.h"
#include <stdexcept>

namespace vhdl {
Signal::Signal(const std::string& name, std::unique_ptr<Type> dataType) : Wire(name, SIGNAL, std::move(dataType)) {}

std::string Signal::dump() const {
	std::string out = "signal " + m_name + " : " + m_dataType->dump();
	return out;
}

std::ostream& operator<<(std::ostream& os, const Signal& signal) {
	os << signal.dump();
	return os;
}

}  // namespace vhdl
