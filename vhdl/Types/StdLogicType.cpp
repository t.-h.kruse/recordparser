#include "StdLogicType.h"
#include <fmt/core.h>

namespace vhdl {

StdLogicType::StdLogicType(char value) : Type(STD_LOGIC), m_value(UNINITIALIZED) {
	if (!isValidStdLogicValue(value)) throw std::runtime_error("'" + std::string(1, value) + "' is no valid StdLogicValue");
}

bool StdLogicType::setValue(char newValue) {
	if (isValidStdLogicValue(newValue)) {
		m_value = static_cast<StdLogicValue>(newValue);
		return true;
	}
	return false;
}

std::string StdLogicType::dump(bool appendDefaultValue) const {
	std::string s = getAsString();
	if (appendDefaultValue) s + " := '" + static_cast<char>(m_value) + "'";
	return s;
}

bool StdLogicType::isValidStdLogicValue(char c) {
	if (c == StdLogicValue::UNINITIALIZED || c == StdLogicValue::UNKNOWN || c == StdLogicValue::LOGIC_0 || c == StdLogicValue::LOGIC_1 ||
		c == StdLogicValue::HIGH_IMPEDANCE || c == StdLogicValue::WEAK || c == StdLogicValue::WEAK_LOW || c == StdLogicValue::WEAK_HIGH)
		return true;
	return false;
}

std::ostream& operator<<(std::ostream& os, const StdLogicType& type) {
	os << type.dump();
	return os;
}

bool operator==(const char& a, const StdLogicType& b) { return a == static_cast<char>(b.value()); }
bool operator!=(const char& a, const StdLogicType& b) { return !(a == b); }

}  // namespace vhdl