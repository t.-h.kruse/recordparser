#ifndef VHDL_STD_LOGIC_VECTOR_TYPE_H_
#define VHDL_STD_LOGIC_VECTOR_TYPE_H_
#include <iostream>
#include <vector>
#include "StdLogicType.h"
#include "Type.h"

namespace vhdl {
class StdLogicVectorType : public Type {
public:
protected:
	std::uint32_t				  m_bitwidth;
	std::uint32_t				  m_bitOffset;
	std::vector<StdLogicType> m_value;

public:
	StdLogicVectorType(std::uint32_t bitwidth);
	StdLogicVectorType(std::uint32_t bitwidth, std::uint32_t bitOffset);
	StdLogicVectorType(const std::string& value, std::uint32_t bitwidth, std::uint32_t bitOffset);
	StdLogicVectorType(const std::string& value, std::uint32_t bitOffset = 0);
	StdLogicVectorType(const StdLogicType::StdLogicValue& value, std::uint32_t bitwidth, std::uint32_t bitOffset = 0);
	StdLogicVectorType(const char value, std::uint32_t bitwidth, std::uint32_t bitOffset = 0);
	~StdLogicVectorType() {}

	std::string dump(bool appendDefaultValue = true) const;
};
std::ostream& operator<<(std::ostream& os, const StdLogicVectorType& type);
}  // namespace vhdl

#endif	// VHDL_STD_LOGIC_VECTOR_TYPE_H_