#ifndef VHDL_TYPE_H_
#define VHDL_TYPE_H_

#include <string>

namespace vhdl {
typedef enum { UNDEFINED, STD_LOGIC, STD_LOGIC_VECTOR } type_t;

class Type {
protected:
	type_t m_type;

public:
	Type();
	Type(type_t type) : m_type(type) {}
	virtual ~Type() {}

	Type& operator=(const Type& rhs);

	std::string getAsString() const;
	std::string dump(bool appendDefaultValue = true) const;	 // curly brackets will be replaced by subclass
};

}  // namespace vhdl

#endif	// VHDL_TYPE_H_