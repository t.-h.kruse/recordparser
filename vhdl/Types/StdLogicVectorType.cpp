#include "StdLogicVectorType.h"
#include <fmt/core.h>

namespace vhdl {
StdLogicVectorType::StdLogicVectorType(std::uint32_t bitwidth) : Type(STD_LOGIC_VECTOR), m_bitwidth(bitwidth), m_bitOffset(0) {
	for (std::uint32_t i = 0; i < bitwidth; i++) {
		m_value.emplace_back();
	}
}

StdLogicVectorType::StdLogicVectorType(std::uint32_t bitwidth, std::uint32_t bitOffset) : Type(STD_LOGIC_VECTOR), m_bitwidth(bitwidth), m_bitOffset(bitOffset) {
	for (std::uint32_t i = 0; i < bitwidth; i++) {
		m_value.emplace_back();
	}
}
StdLogicVectorType::StdLogicVectorType(const std::string& value, std::uint32_t bitwidth, std::uint32_t bitOffset)
	: Type(STD_LOGIC_VECTOR), m_bitwidth(bitwidth), m_bitOffset(bitOffset) {
	for (std::uint32_t i = 0; i < bitwidth; i++) {
		m_value.emplace_back();
	}
	std::size_t numDefaultValuesSpecified = value.size();
	if (numDefaultValuesSpecified > m_value.size()) numDefaultValuesSpecified = m_value.size();
	for (std::size_t i = 0; i < numDefaultValuesSpecified; i++) {
		char c = value[i];
		if (!m_value[i].setValue(c)) throw std::runtime_error("'" + std::string(1, c) + "' is no valid StdLogicValue");
	}
}

StdLogicVectorType::StdLogicVectorType(const std::string& value, std::uint32_t bitOffset) : Type(STD_LOGIC_VECTOR), m_bitOffset(bitOffset) {
	m_bitwidth = static_cast<std::uint32_t>(value.size());
	for (std::uint32_t i = 0; i < m_bitwidth; i++) {
		m_value.emplace_back();
	}
	for (std::uint32_t i = 0; i < m_bitwidth; i++) {
		char c = value[i];
		if (!m_value[i].setValue(c)) throw std::runtime_error("'" + std::string(1, c) + "' is no valid StdLogicValue");
	}
}

StdLogicVectorType::StdLogicVectorType(const StdLogicType::StdLogicValue& value, std::uint32_t bitwidth, std::uint32_t bitOffset)
	: Type(STD_LOGIC_VECTOR), m_bitwidth(bitwidth), m_bitOffset(bitOffset) {
	for (std::uint32_t i = 0; i < m_bitwidth; i++) {
		m_value.emplace_back(value);
	}
}

StdLogicVectorType::StdLogicVectorType(const char value, std::uint32_t bitwidth, std::uint32_t bitOffset)
	: Type(STD_LOGIC_VECTOR), m_bitwidth(bitwidth), m_bitOffset(bitOffset) {
	for (std::uint32_t i = 0; i < m_bitwidth; i++) {
		m_value.emplace_back(value);
	}
}

std::ostream& operator<<(std::ostream& os, const StdLogicVectorType& type) {
	os << type.dump();
	return os;
}

std::string StdLogicVectorType::dump(bool appendDefaultValue) const {
	std::string s = getAsString() + "(" + std::to_string(m_bitwidth + m_bitOffset - 1) + " downto " + std::to_string(m_bitOffset) + ")";
	if (appendDefaultValue) {
		s += " := \"";
		for (std::vector<StdLogicType>::const_reverse_iterator it = m_value.rbegin(); it != m_value.rend(); ++it) {
			s += (static_cast<char>(it->value()));
		}
		s += "\"";
	}
	return s;
}

}  // namespace vhdl