#ifndef VHDL_STD_LOGIC_TYPE_H_
#define VHDL_STD_LOGIC_TYPE_H_
#include <iostream>
#include <stdexcept>
#include "Type.h"

namespace vhdl {
class StdLogicType : public Type {
public:
	typedef enum {
		UNINITIALIZED  = 'U',
		UNKNOWN		   = 'X',
		LOGIC_0		   = '0',
		LOGIC_1		   = '1',
		HIGH_IMPEDANCE = 'Z',
		WEAK		   = 'W',
		WEAK_LOW	   = 'L',
		WEAK_HIGH	   = 'H'
	} StdLogicValue;

protected:
	StdLogicValue m_value;

public:
	StdLogicType() : Type(STD_LOGIC), m_value(UNINITIALIZED) {}
	StdLogicType(StdLogicValue value) : Type(STD_LOGIC), m_value(value) {}
	StdLogicType(char value);
	~StdLogicType() {}

	StdLogicValue value() const { return m_value; }
	void		  setValue(StdLogicValue newValue) { m_value = newValue; }
	bool		  setValue(char newValue);

	std::string dump(bool appendDefaultValue = true) const;

	static bool isValidStdLogicValue(char c);
};
std::ostream& operator<<(std::ostream& os, const StdLogicType& type);

bool operator==(const StdLogicType& a, const StdLogicType& b);
bool operator!=(const StdLogicType& a, const StdLogicType& b);

}  // namespace vhdl

#endif	// VHDL_STD_LOGIC_TYPE_H_