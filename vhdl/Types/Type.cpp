#include "Type.h"
#include "StdLogicType.h"
#include "StdLogicVectorType.h"
namespace vhdl {

Type::Type() : m_type(UNDEFINED) {}

Type& Type::operator=(const Type& rhs) {
	if (&rhs != this) {
		m_type = rhs.m_type;
	}
	return *this;
}

std::string Type::getAsString() const {
	switch (m_type) {
		case STD_LOGIC: return "std_logic";
		case STD_LOGIC_VECTOR: return "std_logic_vector";
		default: return "undefined";
	}
}

std::string Type::dump(bool appendDefaultValue) const {
	switch (m_type) {
		case STD_LOGIC: return static_cast<const StdLogicType*>(this)->dump(appendDefaultValue); break;
		case STD_LOGIC_VECTOR: return static_cast<const StdLogicVectorType*>(this)->dump(appendDefaultValue); break;
		default: return "Type(" + getAsString() + ")";
	}
}
}  // namespace vhdl
