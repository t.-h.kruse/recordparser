#include "Alias.h"
#include "Types.h"

namespace vhdl {
Alias::Alias(const std::string& name, Wire* wireToAlias, uint32_t firstBit, uint32_t lastBit)
	: Wire(name, ALIAS), m_startBit(firstBit), m_endBit(lastBit), m_aliasedWire(wireToAlias) {
	uint32_t width = bitwidth();

	if (width > 1)
		Wire::setType(static_cast<Type*>(new StdLogicVectorType(width)));
	else
		Wire::setType(static_cast<Type*>(new StdLogicType()));
}

uint32_t Alias::bitwidth() const { return m_endBit - m_startBit; }

std::string Alias::dump() const {
	std::string out	  = "alias " + m_name + " : " + m_dataType->dump(false) + " is " + m_aliasedWire->name() + "(";
	uint32_t	width = bitwidth();
	if (width > 1) out += std::to_string(m_endBit) + " downto ";
	out += std::to_string(m_startBit) + ")";
	return out;
}

std::ostream& operator<<(std::ostream& os, const Alias& alias) {
	os << alias.dump();
	return os;
}

}  // namespace vhdl
