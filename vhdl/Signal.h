#ifndef VHDL_SIGNAL_H_
#define VHDL_SIGNAL_H_

#include <iostream>
#include <memory>
#include <string>
#include "Types/Type.h"
#include "Wire.h"

namespace vhdl {
class Signal : public Wire {
public:
	Signal(const std::string& name, std::unique_ptr<Type> dataType);
	~Signal() {}

	std::string dump() const;
};
std::ostream& operator<<(std::ostream& os, const Signal& type);

}  // namespace vhdl

#endif	// VHDL_SIGNAL_H_