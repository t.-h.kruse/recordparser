#ifndef VHDL_ALIAS_H_
#define VHDL_ALIAS_H_

#include <iostream>
#include <memory>
#include <string>
#include "Wire.h"

namespace vhdl {

class Alias : public Wire {
private:
	using Wire::m_dataType;
	using Wire::m_name;

	uint32_t m_startBit;
	uint32_t m_endBit;
	Wire*	 m_aliasedWire;

public:
	Alias(const std::string& name, Wire* wireToAlias, uint32_t firstBit, uint32_t lastBit);

	uint32_t bitwidth() const;
	uint32_t startBit() const { return m_startBit; }
	uint32_t endBit() const { return m_endBit; }
	Wire*	 aliasedWire() const { return m_aliasedWire; }

	std::string dump() const;
};
std::ostream& operator<<(std::ostream& os, const Alias& alias);

}  // namespace vhdl

#endif	// VHDL_ALIAS_H_