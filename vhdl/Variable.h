#ifndef VHDL_VARIABLE_H_
#define VHDL_VARIABLE_H_

#include <iostream>
#include <memory>
#include <string>
#include "Types/Type.h"
#include "Wire.h"

namespace vhdl {
class Variable : public Wire {
public:
	Variable(const std::string& name, std::unique_ptr<Type> dataType);
	~Variable() {}

	std::string dump() const;
};
std::ostream& operator<<(std::ostream& os, const Variable& type);

}  // namespace vhdl

#endif	// VHDL_VARIABLE_H_