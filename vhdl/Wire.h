#ifndef VHDL_WIRE_H_
#define VHDL_WIRE_H_
#include <iostream>
#include <memory>
#include "Types/Type.h"

namespace vhdl {

class Wire {
public:
	typedef enum { SIGNAL, VARIABLE, ALIAS } WireType;

protected:
	std::unique_ptr<Type> m_dataType;
	WireType			  m_wireType;
	std::string			  m_name;

protected:
	Wire(const std::string& name, WireType wireType);
	Wire(const std::string& name, WireType wireType, std::unique_ptr<Type> dataType);

public:
	std::string dump() const;
	void		setType(Type* type);
	std::string name() const { return m_name; }

	WireType type() const { return m_wireType; }
};
std::ostream& operator<<(std::ostream& os, const Wire& type);
}  // namespace vhdl

#endif	// VHDL_WIRE_H_