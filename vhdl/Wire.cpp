#include "Wire.h"
#include "Alias.h"
#include "Signal.h"
#include "Variable.h"

namespace vhdl {

Wire::Wire(const std::string& name, WireType wireType) : m_wireType(wireType), m_name(name) {}

Wire::Wire(const std::string& name, WireType wireType, std::unique_ptr<Type> dataType) : m_dataType(std::move(dataType)), m_wireType(wireType), m_name(name) {}

void Wire::setType(Type* dataType) { m_dataType.reset(dataType); }

std::string Wire::dump() const {
	switch (m_wireType) {
		case SIGNAL: return static_cast<const Signal*>(this)->dump();
		case VARIABLE: return static_cast<const Variable*>(this)->dump();
		case ALIAS: return static_cast<const Alias*>(this)->dump();
	}
}

std::ostream& operator<<(std::ostream& os, const Wire& wire) {
	os << wire.dump();
	return os;
}
}  // namespace vhdl
