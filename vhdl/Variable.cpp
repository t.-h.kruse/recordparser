#include "Variable.h"
#include <stdexcept>

namespace vhdl {
Variable::Variable(const std::string& name, std::unique_ptr<Type> dataType) : Wire(name, SIGNAL, std::move(dataType)) {}

std::string Variable::dump() const {
	std::string out = "variable " + m_name + " : " + m_dataType->dump();
	return out;
}

std::ostream& operator<<(std::ostream& os, const Variable& var) {
	os << var.dump();
	return os;
}

}  // namespace vhdl
