#include <stdint.h>

typedef union {
	unsigned int reg;
	struct {
		unsigned int field1 : 1; /**< comment for field1*/
		unsigned int field2 : 1;
		unsigned int field3 : 4; /**< default: 3; comment for field3*/
		unsigned int field4 : 2; /**< default:2, comment for field4*/
		unsigned int field5 : 4; /**< default: 3; comment for field5*/
		unsigned int field6 : 2; /**< default:2, comment for field6*/
		unsigned int unused1 : 1;
		unsigned int field7 : 1;
		unsigned int : 3;
	};
} status_t;

typedef struct {
	unsigned int enable : 1;  /**< @0xf00dbabe, default=1,Enable description comment*/
	uint16_t	 data	: 16; /**< Default : 0xff, data to store*/
	status_t	 status;	  /**< status of process*/
} volatile example_t;

#ifndef AEGDAC_AVALON_H_
#define AEGDAC_AVALON_H_

#include <stdint.h>

typedef union aegdac_control_u {
	uint32_t reg;
	struct {
		uint8_t enable			   : 1; /**< Default: 0. Enables the peripheral*/
		uint8_t configuration_mode : 1; /**< default : 0*/
		uint8_t continuous_mode	   : 1; /**< default = 1*/
		uint8_t set_value_select   : 1; /**< default=0*/
	};
} aegdac_control_t;

typedef union aegdac_status_u {
	uint32_t reg;
	struct {
		uint8_t range_selected			: 1;
		uint8_t range_switch_enabled	: 1;
		uint8_t wi_hand_enabled			: 1;
		uint8_t							: 0;
		uint8_t dac_low_transmitting	: 1;
		uint8_t dac_low_write_complete	: 1;
		uint8_t dac_low_read_complete	: 1;
		uint8_t dac_low_cc_overflow		: 1;
		uint8_t dac_high_transmitting	: 1;
		uint8_t dac_high_write_complete : 1;
		uint8_t dac_high_read_complete	: 1;
		uint8_t dac_high_cc_overflow	: 1;
	};
} aegdac_status_t;

typedef union aegdac_setvalue_u {
	uint32_t reg[3];
	struct {
		uint32_t terminal  : 21; /**< SetValue sent from terminal*/
		uint32_t backplane : 21;
		uint32_t current   : 21;
	};
} aegdac_setvalue_t;

typedef union aegdac_dac_init_configs_u {
	uint32_t reg;
	struct {
		uint16_t low  : 10;
		uint8_t		  : 0;
		uint16_t high : 10;
	};
} aegdac_dac_init_configs_t;

typedef union aegdac_data_u {
	uint32_t reg;
	struct {
		uint32_t value : 24;
	};
	struct {
		uint32_t data : 20;
		uint8_t	 addr : 3;
		uint8_t	 rw	  : 1;
	} bits;
} aegdac_data_t;

typedef union aegdac_calibration_u {
	uint32_t reg;
	struct {
		uint32_t fullscale : 20;
		int16_t	 zero	   : 10; /**< default = -3. more desc*/
	};
} aegdac_calibration_t;

typedef struct aegdac_avalon_s {
	aegdac_control_t		  control; /**< has the Control bits*/
	aegdac_status_t			  status;
	aegdac_setvalue_t		  setvalue;
	uint32_t				  range_switch_threshold : 21; /**< Default: 0x3ffff, more description*/
	aegdac_dac_init_configs_t dac_init_configs;
	aegdac_data_t			  dac_low_data_in;
	aegdac_data_t			  dac_low_data_out;
	aegdac_calibration_t	  dac_low_calibration;
	aegdac_data_t			  dac_high_data_in;
	aegdac_data_t			  dac_high_data_out;
	aegdac_calibration_t	  dac_high_calibration;

} volatile aegdac_avalon_t;

#endif